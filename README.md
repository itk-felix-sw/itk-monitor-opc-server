# ITK monitor OPC Server

## Requirements 

0. Setup the tdaq environment: can be done with itk-felix-sw/setup.sh

1. Jinja2: can be installed with pip in the user space
 ```
 python -m pip install jinja2 --user
 ```

 2. XSD: installed by superuser on the host
```
yum install xsd
```

