add_library(itk-monitor OBJECT 
            src/ItkMonitor.cpp
	   )

target_include_directories(itk-monitor PUBLIC . 
                           $ENV{TDAQ_INST_PATH}/include 
                           $ENV{TDAQC_INST_PATH}/include 
                           $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/include)

target_link_libraries(itk-monitor is ers ipc owl omniORB4 omnithread)
target_link_directories(itk-monitor PUBLIC
                        $ENV{TDAQ_INST_PATH}/$ENV{CMTCONFIG}/lib
                        $ENV{TDAQC_INST_PATH}/$ENV{CMTCONFIG}/lib)
