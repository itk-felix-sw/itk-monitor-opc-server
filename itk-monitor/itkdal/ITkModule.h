#ifndef ITKMODULE_H
#define ITKMODULE_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * A module inside a powering chain
 * 
 * @author  produced by the IS generator
 */

class ITkModule : public ISInfo {
public:

    /**
     * The type of the run
     */
    std::string                   runType;


    static const ISType & type() {
	static const ISType type_ = ITkModule( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "runType: " << runType << "\t// The type of the run";
	return out;
    }

    ITkModule( )
      : ISInfo( "ITkModule" )
    {
	initialize();
    }

    ~ITkModule(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkModule( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << runType;
    }

    void refreshGuts( ISistream & in ){
	in >> runType;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkModule & info ) {
    info.print( out );
    return out;
}

#endif // ITKMODULE_H
