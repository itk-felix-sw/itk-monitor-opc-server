#ifndef ITKSPCHAIN_H
#define ITKSPCHAIN_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * A serial powering chain
 * 
 * @author  produced by the IS generator
 */

class ITkSPchain : public ISInfo {
public:

    /**
     * The status of the whole FSM
     */
    std::string                   state;

    /**
     * The status of the Low Voltage
     */
    std::string                   LV;

    /**
     * The status of the High Voltage
     */
    std::string                   HV;

    /**
     * The status of the Opto
     */
    std::string                   Opto;

    /**
     * The status of the Interlock
     */
    std::string                   Tilock;

    /**
     * The status of the FELIX core
     */
    std::string                   felixCoreStatus;


    static const ISType & type() {
	static const ISType type_ = ITkSPchain( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "state: " << state << "\t// The status of the whole FSM" << std::endl;
	out << "LV: " << LV << "\t// The status of the Low Voltage" << std::endl;
	out << "HV: " << HV << "\t// The status of the High Voltage" << std::endl;
	out << "Opto: " << Opto << "\t// The status of the Opto" << std::endl;
	out << "Tilock: " << Tilock << "\t// The status of the Interlock" << std::endl;
	out << "felixCoreStatus: " << felixCoreStatus << "\t// The status of the FELIX core";
	return out;
    }

    ITkSPchain( )
      : ISInfo( "ITkSPchain" )
    {
	initialize();
    }

    ~ITkSPchain(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkSPchain( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << state << LV << HV << Opto << Tilock << felixCoreStatus;
    }

    void refreshGuts( ISistream & in ){
	in >> state >> LV >> HV >> Opto >> Tilock >> felixCoreStatus;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkSPchain & info ) {
    info.print( out );
    return out;
}

#endif // ITKSPCHAIN_H
