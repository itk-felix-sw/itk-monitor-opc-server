#ifndef ITKMODULENAMED_H
#define ITKMODULENAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * A module inside a powering chain
 * 
 * @author  produced by the IS generator
 */

class ITkModuleNamed : public ISNamedInfo {
public:

    /**
     * The type of the run
     */
    std::string                   runType;


    static const ISType & type() {
	static const ISType type_ = ITkModuleNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "runType: " << runType << "\t// The type of the run";
	return out;
    }

    ITkModuleNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "ITkModule" )
    {
	initialize();
    }

    ~ITkModuleNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkModuleNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << runType;
    }

    void refreshGuts( ISistream & in ){
	in >> runType;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkModuleNamed & info ) {
    info.print( out );
    return out;
}

#endif // ITKMODULENAMED_H
