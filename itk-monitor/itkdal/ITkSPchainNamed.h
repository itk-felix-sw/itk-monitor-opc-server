#ifndef ITKSPCHAINNAMED_H
#define ITKSPCHAINNAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * A serial powering chain
 * 
 * @author  produced by the IS generator
 */

class ITkSPchainNamed : public ISNamedInfo {
public:

    /**
     * The status of the whole FSM
     */
    std::string                   state;

    /**
     * The status of the Low Voltage
     */
    std::string                   LV;

    /**
     * The status of the High Voltage
     */
    std::string                   HV;

    /**
     * The status of the Opto
     */
    std::string                   Opto;

    /**
     * The status of the Interlock
     */
    std::string                   Tilock;

    /**
     * The status of the FELIX core
     */
    std::string                   felixCoreStatus;


    static const ISType & type() {
	static const ISType type_ = ITkSPchainNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "state: " << state << "\t// The status of the whole FSM" << std::endl;
	out << "LV: " << LV << "\t// The status of the Low Voltage" << std::endl;
	out << "HV: " << HV << "\t// The status of the High Voltage" << std::endl;
	out << "Opto: " << Opto << "\t// The status of the Opto" << std::endl;
	out << "Tilock: " << Tilock << "\t// The status of the Interlock" << std::endl;
	out << "felixCoreStatus: " << felixCoreStatus << "\t// The status of the FELIX core";
	return out;
    }

    ITkSPchainNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "ITkSPchain" )
    {
	initialize();
    }

    ~ITkSPchainNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkSPchainNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << state << LV << HV << Opto << Tilock << felixCoreStatus;
    }

    void refreshGuts( ISistream & in ){
	in >> state >> LV >> HV >> Opto >> Tilock >> felixCoreStatus;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkSPchainNamed & info ) {
    info.print( out );
    return out;
}

#endif // ITKSPCHAINNAMED_H
