#ifndef ITKCHIPNAMED_H
#define ITKCHIPNAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * A chip inside a module
 * 
 * @author  produced by the IS generator
 */

class ITkChipNamed : public ISNamedInfo {
public:

    /**
     * The status of the run
     */
    std::string                   runStatus;

    /**
     * The rate of events
     */
    double                        eventsRate;


    static const ISType & type() {
	static const ISType type_ = ITkChipNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "runStatus: " << runStatus << "\t// The status of the run" << std::endl;
	out << "eventsRate: " << eventsRate << "\t// The rate of events";
	return out;
    }

    ITkChipNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "ITkChip" )
    {
	initialize();
    }

    ~ITkChipNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkChipNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << runStatus << eventsRate;
    }

    void refreshGuts( ISistream & in ){
	in >> runStatus >> eventsRate;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkChipNamed & info ) {
    info.print( out );
    return out;
}

#endif // ITKCHIPNAMED_H
