#ifndef ITKRESOURCE_H
#define ITKRESOURCE_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * An ITK resource
 * 
 * @author  produced by the IS generator
 */

class ITkResource : public ISInfo {
public:

    /**
     * The state of the machine
     */
    std::string                   StateMachine;


    static const ISType & type() {
	static const ISType type_ = ITkResource( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "StateMachine: " << StateMachine << "\t// The state of the machine";
	return out;
    }

    ITkResource( )
      : ISInfo( "ITkResource" )
    {
	initialize();
    }

    ~ITkResource(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkResource( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << StateMachine;
    }

    void refreshGuts( ISistream & in ){
	in >> StateMachine;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkResource & info ) {
    info.print( out );
    return out;
}

#endif // ITKRESOURCE_H
