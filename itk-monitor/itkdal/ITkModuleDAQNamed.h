#ifndef ITKMODULEDAQNAMED_H
#define ITKMODULEDAQNAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Status of the DAQ for each of the 4 chips of a module inside a powering chain
 * 
 * @author  produced by the IS generator
 */

class ITkModuleDAQNamed : public ISNamedInfo {
public:

    /**
     * The type of the run
     */
    std::string                   runType;

    /**
     * The status of the run for chip 1
     */
    std::string                   chip1runStatus;

    /**
     * The rate of events in chip 1
     */
    double                        chip1eventsRate;

    /**
     * The status of the run for chip 2
     */
    std::string                   chip2runStatus;

    /**
     * The status of the run for chip 3
     */
    std::string                   chip3runStatus;

    /**
     * The status of the run for chip 4
     */
    std::string                   chip4runStatus;


    static const ISType & type() {
	static const ISType type_ = ITkModuleDAQNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "runType: " << runType << "\t// The type of the run" << std::endl;
	out << "chip1runStatus: " << chip1runStatus << "\t// The status of the run for chip 1" << std::endl;
	out << "chip1eventsRate: " << chip1eventsRate << "\t// The rate of events in chip 1" << std::endl;
	out << "chip2runStatus: " << chip2runStatus << "\t// The status of the run for chip 2" << std::endl;
	out << "chip3runStatus: " << chip3runStatus << "\t// The status of the run for chip 3" << std::endl;
	out << "chip4runStatus: " << chip4runStatus << "\t// The status of the run for chip 4";
	return out;
    }

    ITkModuleDAQNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "ITkModuleDAQ" )
    {
	initialize();
    }

    ~ITkModuleDAQNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkModuleDAQNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << runType << chip1runStatus << chip1eventsRate << chip2runStatus << chip3runStatus;
	out << chip4runStatus;
    }

    void refreshGuts( ISistream & in ){
	in >> runType >> chip1runStatus >> chip1eventsRate >> chip2runStatus >> chip3runStatus;
	in >> chip4runStatus;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkModuleDAQNamed & info ) {
    info.print( out );
    return out;
}

#endif // ITKMODULEDAQNAMED_H
