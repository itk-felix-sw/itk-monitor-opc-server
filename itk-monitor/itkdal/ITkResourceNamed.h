#ifndef ITKRESOURCENAMED_H
#define ITKRESOURCENAMED_H

#include <is/namedinfo.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * An ITK resource
 * 
 * @author  produced by the IS generator
 */

class ITkResourceNamed : public ISNamedInfo {
public:

    /**
     * The state of the machine
     */
    std::string                   StateMachine;


    static const ISType & type() {
	static const ISType type_ = ITkResourceNamed( IPCPartition(), "" ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISNamedInfo::print( out );
	out << std::endl;
	out << "StateMachine: " << StateMachine << "\t// The state of the machine";
	return out;
    }

    ITkResourceNamed( const IPCPartition & partition, const std::string & name )
      : ISNamedInfo( partition, name, "ITkResource" )
    {
	initialize();
    }

    ~ITkResourceNamed(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkResourceNamed( const IPCPartition & partition, const std::string & name, const std::string & type )
      : ISNamedInfo( partition, name, type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << StateMachine;
    }

    void refreshGuts( ISistream & in ){
	in >> StateMachine;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkResourceNamed & info ) {
    info.print( out );
    return out;
}

#endif // ITKRESOURCENAMED_H
