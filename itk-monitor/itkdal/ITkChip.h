#ifndef ITKCHIP_H
#define ITKCHIP_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * A chip inside a module
 * 
 * @author  produced by the IS generator
 */

class ITkChip : public ISInfo {
public:

    /**
     * The status of the run
     */
    std::string                   runStatus;

    /**
     * The rate of events
     */
    double                        eventsRate;


    static const ISType & type() {
	static const ISType type_ = ITkChip( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "runStatus: " << runStatus << "\t// The status of the run" << std::endl;
	out << "eventsRate: " << eventsRate << "\t// The rate of events";
	return out;
    }

    ITkChip( )
      : ISInfo( "ITkChip" )
    {
	initialize();
    }

    ~ITkChip(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    ITkChip( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << runStatus << eventsRate;
    }

    void refreshGuts( ISistream & in ){
	in >> runStatus >> eventsRate;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const ITkChip & info ) {
    info.print( out );
    return out;
}

#endif // ITKCHIP_H
