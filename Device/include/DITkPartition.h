
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by quasar (https://github.com/quasar-team/quasar/)

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.

    
 */


#ifndef __DITkPartition__H__
#define __DITkPartition__H__

#include <Base_DITkPartition.h>

namespace Device
{

  class DITkPartition:public Base_DITkPartition
  {

  public:
    /* sample constructor */
    explicit DITkPartition (const Configuration::ITkPartition & config,
			    Parent_DITkPartition * parent);
    /* sample dtr */
     ~DITkPartition ();

    /* delegators for
       cachevariables and sourcevariables */


    /* delegators for methods */

  private:
    /* Delete copy constructor and assignment operator */
      DITkPartition (const DITkPartition & other);
      DITkPartition & operator= (const DITkPartition & other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

  public:

  private:



  };

}

#endif // ----------------------------------------------------------------------- *
  __DITkPartition__H__
