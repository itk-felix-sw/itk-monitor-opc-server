#!/usr/bin/env python

import os
import sys
import argparse
import xml.dom.minidom
from colorama import init, Fore
init(autoreset = True)

parser=argparse.ArgumentParser("Make bash scripts")
parser.add_argument("-o","--output",default="startIS",help="output file")
parser.add_argument("-i","--input",default=os.path.dirname(__file__)+"/bin/config.xml",help="input xml file")
args=parser.parse_args()

fw=open(args.output,"w+")
fw.write("#!/bin/bash\n")
fw.write("ipc_server &\n")
fw.write("sleep 1; echo\n")

print(Fore.GREEN + "Parsing %s" % args.input)
doc=xml.dom.minidom.parse(args.input)

print("Find out partition name")
nodes=doc.getElementsByTagName("ISinfo");
if nodes: 
    partition=nodes[0].attributes["partitionName"].value
    server=nodes[0].attributes["serverName"].value
    print("Partition: %s" % partition)
    print("Server: %s" % server)
    fw.write("ipc_server -p %s &\n" % partition)
    fw.write("sleep 1; echo\n")
    fw.write("is_server -p %s -n %s &\n" % (partition,server))
    fw.write("sleep 1; echo\n")
    pass
    
fw.close()
print (Fore.GREEN + "Saved file %s" % args.output)
  
print("Have a nice day")
