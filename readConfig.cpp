// This script reads the config file of the OPC server and create a bash file 
// that can be executed in order to instantiate an IS partition
// with the name and the addressess specified in the config file. 

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "/home/itkpix/Desktop/rapidxml-1.13/rapidxml.hpp"

using namespace std;
using namespace rapidxml;

xml_document<> doc;
xml_node<> * root_node = NULL;
   
int main(void)
{
  string outputFileName = "test";
  ofstream outputFile(outputFileName);
  outputFile << "#!/bin/bash" << endl;
  outputFile << "\nipc_server &" << "\nsleep 1; echo" << endl;

  cout << "Parsing /home/itkpix/itk-felix-sw/itk-monitor-opc-server/build/bin/config.xml" << endl;

  // Read the config.xml file
  ifstream theFile ("/home/itkpix/itk-felix-sw/itk-monitor-opc-server/build/bin/config.xml");
  vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
  buffer.push_back('\0');
   
  // Parse the buffer
  doc.parse<0>(&buffer[0]);

  // Find out the root node
  root_node = doc.first_node("configuration");
  //xml_node <> * partition_node = root_node -> first_node("ItkPartition");
  xml_node <> * partition_node = root_node -> first_node("ISserverITk");
  string partitionName = partition_node -> first_attribute("name") -> value();
  outputFile << "ipc_server -p " << partitionName << " &" << "\nsleep 1; echo" << endl;

  vector<string> addresses;

  // Iterate over the nodes
  for (xml_node<> * resource_node = partition_node -> first_node("ItkResource"); resource_node; resource_node = resource_node -> next_sibling()){
    string address = resource_node -> first_attribute("Address") -> value();
    int pos = address.find(".");
    string addr = address.substr(0, pos);

    if(find(addresses.begin(), addresses.end(), addr) == addresses.end()){
      addresses.push_back(addr);
      outputFile << "is_server -p " << partitionName << " -n " << addr << " &" << "\nsleep 1; echo" << endl;
    }
  }

  outputFile.close();
  cout << "Saved " << outputFileName << endl;
  return 0;
}
