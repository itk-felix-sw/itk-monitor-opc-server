#!/usr/bin/env python

import os
import sys
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("itk_felix_sw_dir",help="itk felix sw path")
parser.add_argument("quasar_project_dir",help="opc ua project path")
parser.add_argument("--build",help="append build to the itk-felix-sw path", action="store_true")
args = parser.parse_args()

files={"itk-monitor/itkdal":["ITkResource.h",
                             "ITkSPchain.h"],
       }


print("Check quasar project exists")
if not os.path.exists(args.quasar_project_dir):
    print ("Project does not exist: %s" % args.opc_project_dir)
    sys.exit()
    pass

print("Check source files exist")
for pkg in files:
    for f in files[pkg]:
        fp="%s/%s/%s" % (args.itk_felix_sw_dir,pkg,f)
        if args.build: fp="%s/build/%s/%s" % (args.itk_felix_sw_dir,pkg,f)
        if not os.path.exists(fp):
            print ("File not found: %s" % fp)
            sys.exit()
            pass
        pass
    pass

print("Copy files to OPC-UA project")
for pkg in files:
    for f in files[pkg]:
        src="%s/%s/%s" % (args.itk_felix_sw_dir,pkg,f)
        if args.build: src="%s/build/%s/%s" % (args.itk_felix_sw_dir,pkg,f)
        dst="%s/%s/%s" % (args.quasar_project_dir,pkg,f)
        if not os.path.exists(os.path.dirname(dst)):
            os.system("mkdir -p %s" % os.path.dirname(dst))
            pass
        os.system("cp -v %s %s" % (src,dst))
        pass
    pass

print("Have a nice day")
